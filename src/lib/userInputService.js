const fs = require('fs');
const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');

const DEFAULT_CHUNK_SIZE = 1000000; // 1 MB
const DEFAULT_MAX_SIZE = 0; // 4 MB
const DEFAULT_PATH = '/tmp/';

const options = [
  {
    name: 'url',
    alias: 'u',
    type: String,
    description: 'url of the target file',
    typeLabel: '[underline]{String}',
  },
  {
    name: 'folder',
    alias: 'f',
    type: String,
    defaultValue: DEFAULT_PATH,
    description: `The folder where the downloaded file to be saved. Default: ${DEFAULT_PATH}`,
    typeLabel: '[underline]{String}',
  },
  {
    name: 'chunkSize',
    alias: 'c',
    type: Number,
    defaultValue: DEFAULT_CHUNK_SIZE,
    description: `The size of file in byte for each chunk. Default value: ${DEFAULT_CHUNK_SIZE}`,
    typeLabel: '[underline]{Number}',
  },
  {
    name: 'max',
    alias: 'm',
    type: Number,
    description: 'The max size of file in byte can be downloaded. Default value: Unlimited',
    typeLabel: '[underline]{Number}',
  },
  {
    name: 'debug',
    alias: 'd',
    type: Boolean,
    defaultValue: false,
    description: 'Debug mode',
    typeLabel: '[underline]{Boolean}',
  },
  {
    name: 'help',
    type: Boolean,
    description: 'Display the help menu.',
  },
];

const sections = [
  {
    header: 'download-booster',
    content: 'Download made easy',
  },
  {
    header: 'Options',
    optionList: options,
  },
];

// Extract user args
exports.getUserInput = () => {
  let args = {};
  args = commandLineArgs(options);

  if (args.help) {
    console.log(commandLineUsage(sections));
    process.exit(0);
  }

  function getFileName(url) {
    const tokens = url.split('/');
    return tokens[tokens.length - 1];
  }

  if (!args.url) {
    console.error('Error: You must specify a url.');
    console.log(commandLineUsage(sections));
    process.exit(1);
  }

  if (args.max <= 0 || isNaN(args.max) || args.chunkSize <= 0 || isNaN(args.chunkSize)) {
    console.error('Error: Download file size should be an number greater than 0.');
    console.log(commandLineUsage(sections));
    process.exit(1);
  }

  // Calculate user settings
  args.path = [args.folder, getFileName(args.url)].join('/');
  if (fs.existsSync(args.path)) {
    console.log('[WARNING] File exist! It will be overwritten');
  }
  return args;
};
