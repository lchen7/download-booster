const request = require('request');
const Pace = require('pace');
const q = require('q');

// Get the length of targeting source.
exports.getContentLength = (url) => {
  const deferred = q.defer();
  request.head(url, (error, response) => {
    if (error) { deferred.reject(error); }
    if (!error && response.statusCode === 200) {
      deferred.resolve(parseInt(response.headers['content-length'], 10));
    }
    if (!error && response.statusCode !== 200) {
      deferred.reject(response.statusCode);
    }
  });
  return deferred.promise;
};

// Create chunck request option for given range.
function createChunckRequestOptions(url, startOfRange, endOfRange) {
  return {
    url,
    headers: {
      Range: `bytes=${startOfRange}-${endOfRange}`,
    },
    encoding: null,
  };
}

// Generate options list for all chunck request for given chunk size and max size specified.
exports.getRequestOptionsList = (url, chunkSize, maxSize) => {
  let currentRangeStart = 0;
  const optionsList = [];
  while (currentRangeStart < maxSize) {
    const rangeEnd = Math.min(currentRangeStart + chunkSize - 1, maxSize - 1);
    optionsList.push(createChunckRequestOptions(url, currentRangeStart, rangeEnd));
    currentRangeStart += chunkSize;
  }
  return optionsList;
};

// Request the chunck range specified in the options.
function requestChunck(options, pace) {
  const deferred = q.defer();
  request(options, (error, response) => {
    if (error) {
      deferred.reject(error);
    }
    if (!error && response.statusCode === 206) {
      pace.op();
      deferred.resolve(response);
    }
    if (!error && response.statusCode !== 206) {
      deferred.reject(response.statusCode);
    }
  });
  return deferred.promise;
}

exports.sendRequests = (optionsList) => {
  const pace = new Pace(optionsList.length);
  const requests = optionsList.map(options => requestChunck(options, pace));
  return Promise.all(requests);
};

// Calculate the total buffer size of final file.
function getTotalBUfferSize(responses) {
  const reducer = (size, response) => size + parseInt(response.headers['content-length'], 10);
  return responses.reduce(reducer, 0);
}

// Merge result into one buffer.
exports.mergeResult = (responses) => {
  const buffer = new Uint8Array(getTotalBUfferSize(responses));
  let currentSize = 0;
  responses.forEach((res) => {
    const currentBuffer = new Uint8Array(res.body);
    const currentResSize = parseInt(res.headers['content-length'], 10);
    buffer.set(currentBuffer, currentSize);
    currentSize += currentResSize;
  });
  return Buffer.from(buffer);
};
