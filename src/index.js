#!/usr/bin/env node

const fs = require('fs');
const requestHelper = require('./lib/requestHelper');
const userInputService = require('./lib/userInputService');

const args = userInputService.getUserInput();

function getDownloadOptions(fileSize) {
  const totalSize = (args.max === undefined || args.max > fileSize) ? fileSize : args.max;
  return requestHelper.getRequestOptionsList(args.url, args.chunkSize, totalSize);
}

function saveResult(result) {
  fs.writeFileSync(`${args.path}`, result);
}

function raiseError(err) {
  throw new Error(`Received unexpected http code ${err}`);
}

requestHelper.getContentLength(args.url, raiseError)
  .then(getDownloadOptions)
  .then(requestHelper.sendRequests, raiseError)
  .then(requestHelper.mergeResult)
  .then(saveResult)
  .catch((err) => {
    console.log('Download process failed.');
    console.log(`${err}`);
  });

