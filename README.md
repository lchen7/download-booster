# download booster

Target: Download booster is an application that downloads part of a file from a web server, in chunks.

Context: This is a simplified version of a “download booster”, which speeds up downloads by requesting
files in multiple pieces simultaneously (saturating the network), then reassembling the pieces.

Source:
```
The source lives in https://bitbucket.org/lchen7/download-booster.
```

Installation:
```
1. npm install
2. npm link
3. Good to run!
```

Run:
```
1. downloadBooster
2. or `./src/index.js`
3. Then follow the instruction.
```

download-booster

  Download made easy

Options
```

  -u, --URL String         URL of the target file

  -f, --folder String      The folder where the downloaded file to be saved. Default: /tmp/download-booster/

  -c, --chunkSize Number   The size of a file in byte for each chunk. Default value: 1000000

  -m, --max Number         The max size of file in byte can be downloaded. Default value: Unlimited

  --help                   Display the help menu.
```

Following are the list of things I would like to have:

```
TODO: Giving user option to change the file name

TODO: Ask user input when the file exits.

TODO: URL validation.
```
Limitation:
```
    1. Currently, this application support parallel downloads only.

    2. Since the max size file download has been removed. Not providing a max args will download the entire file.
```